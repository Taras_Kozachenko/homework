import java.util.Scanner;

public class TwoSix {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите число");

        int x = scan.nextInt();
        while (x > 0) {
            System.out.print(x % 10);
            x /= 10;
        }
    }
}
