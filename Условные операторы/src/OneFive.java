import java.util.Scanner;

public class OneFive {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите количество балов:");
        int rating = scan.nextInt();

        if (rating >=0 && rating <=19){
            System.out.println("Оценка F");
        }
        if (rating >=20 && rating <= 39){
            System.out.println("Оценка E");
        };
        if (rating >= 40 && rating <= 59){
            System.out.println("Оценка D");
        }
        if (rating >= 60 && rating <= 74){
            System.out.println("Оценка C");
        }
        if (rating >= 75 && rating <= 89){
            System.out.println("Оценка B");
        }
        if (rating >= 90 && rating <= 100){
            System.out.println("Оценка A");
        }
    }
}
