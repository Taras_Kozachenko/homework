import java.io.InputStreamReader;
import java.util.Random;
public class OneThree {
    private final static int MAX_NUMBER = 3;
    public static void main(String[] args)
    {
        int count = readCount();
        int[] numbers = generateNumbers(count);
        printNumbers(numbers);
        int positiveSum = 0;

        for(int i = 0; i < numbers.length; i++)
        {
            if(numbers[i] > 0)
                positiveSum += numbers[i];
        }

        System.out.println("Positive Sum = " + positiveSum);
    }

    private static int readCount()
    {
        System.out.print("Input count: ");
        try
        {
            char[] buf = new char[100];
            InputStreamReader reader = new InputStreamReader(System.in);
            int cnt = reader.read(buf);
            String str = new String(buf, 0, cnt - 1);
            return Integer.parseInt(str);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return -1;
        }
    }

    private static void printNumbers(int[] numbers)
    {
        System.out.print("numbers: ");
        for(int i = 0; i < numbers.length; i++)
        {
            System.out.print(numbers[i]);
            if(i < numbers.length - 1)
                System.out.print(", ");
        }
        System.out.println("");
    }

    private static int[] generateNumbers(int count)
    {
        int[] numbers = new int[count];

        Random r = new Random();
        for(int i = 0; i < count; i++)
        {
            int number = r.nextInt(MAX_NUMBER);

            if(r.nextBoolean())
                number = -number;

            numbers[i] = number;
        }

        return numbers;
    }
}
