import javax.swing.*;
import java.util.Scanner;

public class OneTwo {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите координаты x:");
        int x = scan.nextInt();
        System.out.println("Введите координаты y:");
        int y = scan.nextInt();

        if (x>0 && y>0){
            System.out.println("Точка с координатами принадлежит первой четверти");
        }
        if (x>0 && y<0){
            System.out.println("Точка с координатами принадлежит второй четверти");
        }
        if (x<0 && y<0){
            System.out.println("Точка с координатами принадлежит третьей четверти");
        }
        if (x<0 && y>0){
            System.out.println("Точка с координатами принадлежит четвёртой четверти");
        }
    }
}
