public class FourFour {
    public static void main(String[] args) {
        Point p1 = new Point(0,0);
        Point p2 = new Point(10,10);
        System.out.println(p1.distance(p2));
        System.out.println(p2.distance(p1));
    }
}
class Point{
    private double x;
    private double y;
    public Point(double x1, double y1) {
        this.x = x1;
        this.y = y1;
    }
    public double distance(Point p2){
        return Math.sqrt((p2.x - this.x) * (p2.x - this.x) + (p2.y - this.y) * (p2.y - this.y));
    }
    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
