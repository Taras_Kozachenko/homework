import java.util.Scanner;

public class FourOne {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите номер дня недели:");
        int dayNumber = scan.nextInt();

        if (dayNumber == 1) {
            System.out.println("Сегодня - понедельник");
        }
        else if (dayNumber == 2) {
            System.out.println("Сегодня - вторник");
        }
        else if (dayNumber == 3) {
            System.out.println("Сегодня - среда");
        }
        else if (dayNumber == 4) {
            System.out.println("Сегодня - четверг");
        }
        else if (dayNumber == 5) {
            System.out.println("Сегодня - пятница");
        }
        else if (dayNumber == 6) {
            System.out.println("Сегодня - суббота");
        }
        else if (dayNumber == 7) {
            System.out.println("Сегодня - восскресенье");
        }
        else {
            System.out.println("Такого дня недели не существует");
        }


    }
}
