import java.util.Arrays;

public class ThreeFour {
    static double max = 0;
    private static double array[] = { 0.11, 0.2, 0.33, 2.7, 1.08, 5.4 };

    public static void main(String[] args) {
        findMax(array);
    }

    public static void findMax(double[] array) {
        Arrays.sort(array);
        max = array[array.length - 1];
        System.out.println("Максимальный индекс массива: " +max);
    }
}
