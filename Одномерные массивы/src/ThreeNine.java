import java.util.Arrays;

public class ThreeNine {
    /*Сортировка массива методом Selection*/
    public static void main(String[] args) {
        int[] arrSelection = {10, 2, 10, 3, 1, 2, 5};
        System.out.println("Неотсортированый массив: " + Arrays.toString(arrSelection));

        for (int i = 0; i < arrSelection.length; i++) {
            int min = arrSelection[i];
            int min_i = i;
            for (int j = i + 1; j < arrSelection.length; j++) {
                if (arrSelection[j] < min) {
                    min = arrSelection[j];
                    min_i = j;
                }
            }
            if (i != min_i) {
                int tmp = arrSelection[i];
                arrSelection[i] = arrSelection[min_i];
                arrSelection[min_i] = tmp;
            }
        }
        System.out.print("Отсортированый массив методом Selection: " + Arrays.toString(arrSelection) + "\n");

        /*Сортировка массива методом Bubble*/
        for (int i = arrSelection.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arrSelection[j] > arrSelection[j + 1]) {
                    int tmp = arrSelection[j];
                    arrSelection[j] = arrSelection[j + 1];
                    arrSelection[j + 1] = tmp;
                }
            }
        }
        System.out.print("Отсортированый массив методом Bubble: " + Arrays.toString(arrSelection) + "\n");

        /*Сортировка массива методом Insert*/
        for (int left = 0; left < arrSelection.length; left++) {

            int value = arrSelection[left];
            int i = left - 1;
            for (; i >= 0; i--) {
                if (value < arrSelection[i]) {
                    arrSelection[i + 1] = arrSelection[i];
                } else {
                    break;
                }
            }
            arrSelection[i + 1] = value;
        }
        System.out.print("Отсортированый массив методом Insert: " + Arrays.toString(arrSelection) + "\n");
    }
}
