import java.util.Arrays;
import java.util.Random;


public class ThreeSeven {
    public static void main(String[] args) {
        int [] arr = makeRandomArr(10, 10);
        int sum = 0;
        int n = 0;

        for ( int i = 0; i < arr.length; i++ )
        {
            if ( arr[i] % 2 != 0 )
            {
                ++n;
                sum += arr[i];
            }
        }
        System.out.println("Массив: " + Arrays.toString(arr));
        System.out.println( "Всего нечетных элементов " + n );
        System.out.println( "Их сумма равна " + sum );
    }
    private static int[] makeRandomArr(int size, int bound) {
        int[] arr = new int[size];
        Random rnd = new Random();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rnd.nextInt(bound);
        }
        return arr;
    }
}
