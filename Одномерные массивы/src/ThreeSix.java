import java.util.Arrays;
import java.util.Random;

public class ThreeSix {
    public static void main(String[] args) {

        int[] myArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        int size = myArray.length;

        for (int i = 0; i < size / 2; i++) {
            int temp = myArray[i];
            myArray[i] = myArray[size - 1 - i];
            myArray[size - 1 - i] = temp;
        }
        System.out.println("Массив после реверса: ");
        System.out.println(Arrays.toString(myArray));
    }
}
