public class ThreeTenShell {
    public static void main(String[] args) {
        int[] mas = new int[12];
        for (int i = 0; i < mas.length; i++) {
            mas[i] = (int) Math.round(Math.random() * 100);
            System.out.print(mas[i] + " ");
        }
        //Algorithm
        int d = mas.length/2;
        while (d > 0){
            for (int i = 0; i < (mas.length - d); i++){
                int j = i;
                while((j >= 0) && (mas[j] > mas[j + d])){
                    int temp = mas [j];
                    mas[j] = mas[j + d];
                    mas[j + d] = temp;
                    j--;
                }
            }
            d = d/2;
        }
        //output date
        System.out.println ();
        for (int i =0; i < mas.length; i++) System.out.print(mas[i] + " ");
    }
}